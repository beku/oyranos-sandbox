/** @file  oyranos_git_version.h
 *  @brief automatic generated variables
 */
#ifndef OYRANOS_GIT_VERSION_H
#define OYRANOS_GIT_VERSION_H

#define OY_GIT_VERSION                 "0.9.6-2716-g09d47b78c-2022-04-07"  /**< git describe --always HEAD  + date */ 


#endif /*OYRANOS_GIT_VERSION_H*/


